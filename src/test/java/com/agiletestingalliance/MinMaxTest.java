package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class MinMaxTest {

@Test
public void testGreaterIs() {
        MinMax minMax = new MinMax();
        assertEquals(minMax.greaterIs(1,2), 2);
        assertEquals(minMax.greaterIs(2,1), 2);
    }

@Test
public void testBar() {
        MinMax minMax = new MinMax();
        assertEquals(minMax.bar(""), "");
        assertEquals(minMax.bar("test"), "test");
    }

}
